# **Projeto IU Pay**
No projeto foi escolhido o java devido a familiaridade com a linguagem.

## **Setup**
Para esse projeto utilizaremos a jdk do java 11.0.6

No linux:
```
sudo apt install default-jdk
```
O projeto foi feito utilizando a ide do Eclipse neon EE, nele foi selecionado o servidor [tomcat 9.0.31](https://tomcat.apache.org/download-90.cgi).

Apos isso foi configurado um novo projeto maven com o archetype: jersey-quickstart-webapp.

Para isso foi preciso mudar o pom.xml em todas as ocorrencias de ${jersey.version}:
```
        <dependency>
            <groupId>com.sun.jersey</groupId>
            <artifactId>jersey-client</artifactId>
            <version>${jersey.version}</version>
            <scope>test</scope>
        </dependency>   
```
Para:
```
        <dependency>
            <groupId>com.sun.jersey</groupId>
            <artifactId>jersey-client</artifactId>
            <version>1.19.1</version>
            <scope>test</scope>
        </dependency> 
```
Tambem e preciso adicionar a dependencia do Json:
```
<dependency>
    <groupId>javax.json</groupId>
    <artifactId>javax.json-api</artifactId>
    <version>1.0</version>
</dependency>
```
Para terminar de configurar o banco, precisamos de um [mysql connector](https://mvnrepository.com/artifact/mysql/mysql-connector-java).
A escolha foi pelo [5.1.38](https://mvnrepository.com/artifact/mysql/mysql-connector-java/5.1.38):
```
<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.38</version>
</dependency>
```

Para fazer o projeto de teste Utilizamos o archetype: maven-archetype-quickstart.

Utilizamos a seguintes dependencias:
```
<dependencies>
  	<dependency>
     <groupId>org.hamcrest</groupId>
     <artifactId>hamcrest-all</artifactId>
     <version>1.3</version>
     <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>rest-assured</artifactId>
      <version>3.0.6</version>
    </dependency>
    <dependency>
	   <groupId>org.mybatis</groupId>
	   <artifactId>mybatis</artifactId>
	   <version>3.4.5</version>
	</dependency>
    <dependency>
      <groupId>com.github.fge</groupId>
      <artifactId>json-schema-validator</artifactId>
      <version>2.2.6</version>
    </dependency>
    <dependency>
	 <groupId>mysql</groupId>
	 <artifactId>mysql-connector-java</artifactId>
	 <version>5.1.38</version>
	</dependency>
    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>json-path</artifactId>
      <version>3.0.6</version>
    </dependency>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
```

A classe de test e executada como Junit.
O arquivo gerenateDataBase.sql possui todo o setup necessario para o banco de dados.
O arquivo Dump.sql e utilizado pela classe de teste.