package com.automation.APITest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import java.sql.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.Test;

import io.restassured.http.ContentType;


public class AppTest 
{
    
	private String uriBase = "http://localhost:8080/rest/Api";
	
	@Test
	public void Teste() throws Exception 
	{
		try {
			this.resetBank();
		}catch(Exception e) {
			throw e;
		}
		
		this.sendPost("A", "B", "10.00");
		this.sendPost("A", "D", "20.00");
		this.sendPost("C", "A", "30.00");
		this.sendPost("B", "D", "40.00");
		this.sendPost("D", "C", "80.00");
		this.sendPost("D", "B", "30.00");
		this.sendPost("E", "A", "120.00");
		
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();
		Transaction transaction = null;
		
		transaction = new Transaction("E", "A", "120.0");
		transactions.add(transaction);
		
		transaction = new Transaction("D", "C", "50.0");
		transactions.add(transaction);
		
		this.recieveGet(transactions);
	}
	
	private void recieveGet(ArrayList<Transaction> transactions) 
	{
		for(int i = 0; i < transactions.size(); i++) 
		{
			given()
				.relaxedHTTPSValidation()
			.when()
				.get(uriBase + "/Balance")
			.then()
				.statusCode(200)
				.contentType(ContentType.JSON)
				.body("originBank[" + i + "]", is(transactions.get(i).getOriginBank()))
				.body("destinyBank[" + i + "]", is(transactions.get(i).getDestinyBank()))
				.body("price[" + i + "]", is(transactions.get(i).getPrice()));
		}
	}
	
	private void sendPost(String originBank, String destinyBank, String price) 
	{
		given()
			.relaxedHTTPSValidation()
			.header("Content-Type","application/json")
			.body
			(
				this.generateJson(originBank, destinyBank, price)
			)
		.when()
			.post(uriBase + "/Payment")
		.then()
			.statusCode(204);
	}
	
	private String generateJson(String originBank, String destinyBank, String price) 
	{
		String response = 
			"{\n"
				+ "\"originBank\": \"" + originBank + "\",\n"
				+ "\"destinyBank\": \"" + destinyBank + "\",\n"
				+ "\"price\": \"" + price + "\"\n"
			+ "}";
		
		return response;
	}
	
	private void resetBank() throws Exception 
	{
		Connection con = null;
		
		String url = "jdbc:mysql://127.0.0.1:3306/iupay?allowMultiQueries=true";
		String user = "root";
		String pwd = "";
		
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, user, pwd);
			ScriptRunner sr = new ScriptRunner(con);
			Reader reader = new BufferedReader(new FileReader("/home/iago/workspace/rest/Dump.sql"));
			sr.runScript(reader);
			
			Statement stmt = con.createStatement();
			
			String populate_balance = "CREATE TRIGGER populate_balance "
					+ "AFTER INSERT "
					+ "ON banks FOR EACH ROW "
					+ "BEGIN "
						
						+ "DECLARE done INT DEFAULT FALSE; "
						+ "DECLARE bank CHAR(16); "
						+ "DECLARE old_banks CURSOR FOR SELECT bank_name FROM banks WHERE id < NEW.id; "
						+ "DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE; "
						
						+ "OPEN old_banks; "
						
						+ "read_loop: LOOP "
							+ "FETCH old_banks INTO bank; "
							+ "IF done THEN "
								+ "LEAVE read_loop; "
							+ "END IF; "
							+ "INSERT INTO balance (origin_bank, destiny_bank, price) VALUES (NEW.bank_name, bank, 0); "
						+ "END LOOP; "
						
						+ "CLOSE old_banks; "
					+ "END";
			
			String update_balance = "CREATE TRIGGER update_balance "
					+ "AFTER INSERT "
					+ "ON payments FOR EACH ROW " 
					+ "BEGIN "
						+ "IF (SELECT id FROM banks WHERE NEW.origin_bank = banks.bank_name) < (SELECT id FROM banks WHERE NEW.destiny_bank = banks.bank_name) THEN "
							+ "UPDATE balance SET balance.price = (balance.price + NEW.price) "
							+ "WHERE balance.origin_bank = NEW.origin_bank "
							+ "AND balance.destiny_bank = NEW.destiny_bank; "
						+ "ELSE "
							+ "UPDATE balance SET balance.price = (balance.price - NEW.price) "
							+ "WHERE balance.origin_bank = NEW.destiny_bank "
							+ "AND balance.destiny_bank = NEW.origin_bank; "
						+ "END IF; "
					+ "END";
			
			String update_banks = "CREATE TRIGGER update_banks "
					+ "AFTER UPDATE "
					+ "ON balance FOR EACH ROW "
					+ "BEGIN "
						+ "UPDATE banks SET banks.sum = ( "
							+ "(SELECT COALESCE(SUM(price),0) FROM balance WHERE origin_bank = NEW.origin_bank) + "
							+ "(SELECT COALESCE(-SUM(price),0) FROM balance WHERE destiny_bank = NEW.origin_bank) "
						+ ") "
						+ "WHERE banks.bank_name = NEW.origin_bank; "
						+ "UPDATE banks SET banks.sum = ( "
							+ "(SELECT COALESCE(SUM(price),0) FROM balance WHERE origin_bank = NEW.destiny_bank) + "
							+ "(SELECT COALESCE(-SUM(price),0) FROM balance WHERE destiny_bank = NEW.destiny_bank) "
						+ ") "
						+ "WHERE banks.bank_name = NEW.destiny_bank; "
					+ "END";
			
		    stmt.execute(populate_balance);
		    stmt.execute(update_balance);
		    stmt.execute(update_banks);
			
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			throw e;
		}
	}
}
