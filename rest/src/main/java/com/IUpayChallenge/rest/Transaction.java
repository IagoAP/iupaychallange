package com.IUpayChallenge.rest;

public class Transaction {
    
	private String originBank;
    private String destinyBank;
    private String price;

    public Transaction()
    {
    	//for the object have a custom constructor and to be a json class it needs to have explicitly the empty constructor
    }
    
    public Transaction(String nOriginBank, String nDestinyBank, Double nPrice)
    {
    	originBank = nOriginBank;
    	destinyBank = nDestinyBank;
        price = nPrice.toString();
    }

    public String toString()
    {
        return "Pagamento de " + originBank + " para " + destinyBank + " no preço de: " + price;
    }
    

    public String getOriginBank() 
    {
		return originBank;
	}

	public void setOriginBank(String originBank) 
	{
		this.originBank = originBank;
	}

	public String getDestinyBank() 
	{
		return destinyBank;
	}

	public void setDestinyBank(String destinyBank) 
	{
		this.destinyBank = destinyBank;
	}

	public String getPrice() 
	{
		return price;
	}
	
	public void setPrice(String price) 
	{
		this.price = price;
	}
    
}
