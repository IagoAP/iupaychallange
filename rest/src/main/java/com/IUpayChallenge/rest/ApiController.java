
package com.IUpayChallenge.rest;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/Api")
public class ApiController {
    
    private RepositoryController rc = new RepositoryController();

    @GET
    @Path("/Balance")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Transaction> getResult() throws Exception 
    {
    	try 
    	{
    		ArrayList<String> banks = rc.getBanks();
			ArrayList<Double> sums = rc.getSums();
			return new TableObject(banks, sums).transactions();
		} 
    	catch (Exception e) 
    	{
			e.printStackTrace();
			throw e;
		}	
    }

    @POST
    @Path("/Payment")
    @Consumes(MediaType.APPLICATION_JSON)
    public void setPayment(Transaction transaction) throws Exception 
    {
    	try{
    		rc.insertPayment(transaction.getOriginBank(), transaction.getDestinyBank(), transaction.getPrice());
    	}
    	catch (Exception e) 
    	{
    		e.printStackTrace();
			throw e;
        }
    }
}
