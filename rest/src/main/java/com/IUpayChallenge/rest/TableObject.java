package com.IUpayChallenge.rest;

import java.util.ArrayList;

public class TableObject {
    
    private ArrayList<String> banks = new ArrayList<String>();
    private ArrayList<Double> values = new ArrayList<Double>();
    private ArrayList<Transaction> balance = new ArrayList<Transaction>();

    public TableObject(ArrayList<String> nBanks, ArrayList<Double> nValues)
    {
        this.values = nValues;
        this.banks = nBanks;
    }
    
    public ArrayList<Transaction> transactions()
    {
    	while(!this.finished()) 
    	{
    		int big = 0;
        	int small = 0;
        	Double bigVal = 0.0;
        	Double smallVal = 0.0;
        	
	    	for(int i = 0; i < values.size(); i++) 
	    	{
	    		if(values.get(i) != 0) 
	    		{
		    		if(values.get(i) > bigVal) 
		    		{
		    			bigVal = values.get(i);
		    			big = i;
		    		}
		    		if(values.get(i) < smallVal) 
		    		{
		    			smallVal = values.get(i);
		    			small = i;
		    		}
	    		}
	    	}
	    	
	    	this.addBalance(bigVal, big, smallVal, small);
    	}
    	
    	return balance;
    }

    private void addBalance(Double bigVal, int big, Double smallVal, int small) 
    {
    	Transaction transaction;
		Double diff = bigVal + smallVal; //the small value comes negative from the database
		if(diff > 0) 
		{
			transaction = new Transaction(banks.get(big), banks.get(small), bigVal-diff);
			values.set(big, diff);
			values.set(small, 0.0);
		}
		else if(diff < 0) 
		{
			transaction = new Transaction(banks.get(big), banks.get(small), bigVal);
			values.set(big, 0.0);
			values.set(small, diff);
		}else 
		{
			transaction = new Transaction(banks.get(big), banks.get(small), bigVal);
			values.set(big, 0.0);
			values.set(small, 0.0);
		}
		
		balance.add(transaction);
		
	}

	public Boolean finished() 
	{
    	for(Double value : values) 
    	{
    		if(value != 0) 
    		{
    			return false;
    		}
    	}
    	
    	return true;
    }

}
