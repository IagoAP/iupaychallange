package com.IUpayChallenge.rest;

import java.sql.*;
import java.util.ArrayList;

public class RepositoryController {


	Connection con = null;
	
	//Defining connection
	public RepositoryController()
	{
		String url = "jdbc:mysql://127.0.0.1:3306/iupay";
		String user = "root";
		String pwd = "";
		
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, user, pwd);
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}

	//Payment table
	public void insertPayment(String originBank, String destinyBank, String price) throws Exception
	{
		
		String sql = "INSERT INTO payments (origin_bank, destiny_bank, price) VALUES (?,?,?)";
		
		try 
		{
			this.validBank(originBank);
			this.validBank(destinyBank);

			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, originBank);
			st.setString(2, destinyBank);
			st.setDouble(3, Double.parseDouble(price));
			st.executeUpdate();
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			throw e;
		}
	}

	//Banks table
	private void validBank(String bank) throws Exception
	{
		
		String sql = "SELECT * FROM banks WHERE bank_name = '" + bank + "'";
		
		try 
		{
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				return;
			}
			
			this.insertBank(bank);
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			throw e;
		}
	}

	private void insertBank(String bank) throws Exception
	{

		String sql = "INSERT INTO banks (bank_name) VALUES (?)";
		
		try 
		{
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, bank);
			st.executeUpdate();
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			throw e;
		}

	}

	public ArrayList<String> getBanks() throws Exception
	{

		String sql = "SELECT bank_name FROM banks ORDER BY id ASC";
		ArrayList<String> banks = new ArrayList<String>();

		try 
		{
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				banks.add(rs.getString(1));
			}
			
			return banks;
		}catch(Exception e) 
		{
			e.printStackTrace();
			throw e;
		}
		
	}
	
	public ArrayList<Double> getSums() throws Exception
	{

		String sql = "SELECT sum FROM banks";
		ArrayList<Double> banks = new ArrayList<Double>();

		try 
		{
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				banks.add(rs.getDouble(1));
			}
			
			return banks;
		}catch(Exception e) 
		{
			e.printStackTrace();
			throw e;
		}
		
	}
}
