DROP DATABASE IF EXISTS iupay;

CREATE DATABASE iupay;

USE iupay;

create table banks (
	       id INT(10) UNSIGNED AUTO_INCREMENT , 
	bank_name VARCHAR(255) NOT NULL, 
	      sum DOUBLE(10,2) NOT NULL DEFAULT 0.00,
	      
	primary key(
		id, 
		bank_name
	)
);

CREATE INDEX bank_name ON banks(bank_name);

ALTER TABLE banks ADD UNIQUE (bank_name);

create table payments (
	          id INT(10) UNSIGNED AUTO_INCREMENT , 
	 origin_bank VARCHAR(255) NOT NULL, 
	destiny_bank VARCHAR(255) NOT NULL, 
	       price DOUBLE(10,2) UNSIGNED NOT NULL, 
	
	primary key(id)
);

alter table payments add constraint bank_origin foreign key ( origin_bank ) references banks ( bank_name );

alter table payments add constraint bank_destiny foreign key ( destiny_bank ) references banks ( bank_name );

CREATE TABLE balance ( 
	origin_bank VARCHAR(255) NOT NULL, 
	destiny_bank VARCHAR(255) NOT NULL, 
	price DOUBLE(10,2) SIGNED NOT NULL 
);

alter table balance add constraint bank_fisrt foreign key ( origin_bank ) references banks ( bank_name );

alter table balance add constraint bank_second foreign key ( destiny_bank ) references banks ( bank_name );

DELIMITER $$

CREATE TRIGGER populate_balance
AFTER INSERT
ON banks FOR EACH ROW 
BEGIN
	
	DECLARE done INT DEFAULT FALSE;
	DECLARE bank CHAR(16);
	DECLARE old_banks CURSOR FOR SELECT bank_name FROM banks WHERE id < NEW.id;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	OPEN old_banks;
	
	read_loop: LOOP
		FETCH old_banks INTO bank;
		IF done THEN
			LEAVE read_loop;
		END IF;
		INSERT INTO balance (origin_bank, destiny_bank, price) VALUES (NEW.bank_name, bank, 0);
	END LOOP;
	
	CLOSE old_banks;
END$$
 
DELIMITER ;

DELIMITER $$

CREATE TRIGGER update_balance
AFTER INSERT
ON payments FOR EACH ROW 
BEGIN
	
	IF (SELECT id FROM banks WHERE NEW.origin_bank = banks.bank_name) > (SELECT id FROM banks WHERE NEW.destiny_bank = banks.bank_name) THEN
		UPDATE balance SET balance.price = (balance.price + NEW.price)
		WHERE balance.origin_bank = NEW.origin_bank
		AND balance.destiny_bank = NEW.destiny_bank;
	ELSE
		UPDATE balance SET balance.price = (balance.price - NEW.price)
		WHERE balance.origin_bank = NEW.destiny_bank
		AND balance.destiny_bank = NEW.origin_bank;
	END IF;
	
END$$
 
DELIMITER ;

DELIMITER $$

CREATE TRIGGER update_banks
AFTER UPDATE
ON balance FOR EACH ROW 
BEGIN
	UPDATE banks SET banks.sum = (
		(SELECT COALESCE(SUM(price),0) FROM balance WHERE origin_bank = NEW.origin_bank) +
		(SELECT COALESCE(-SUM(price),0) FROM balance WHERE destiny_bank = NEW.origin_bank)
	)
	WHERE banks.bank_name = NEW.origin_bank;
	UPDATE banks SET banks.sum = (
		(SELECT COALESCE(SUM(price),0) FROM balance WHERE origin_bank = NEW.destiny_bank) +
		(SELECT COALESCE(-SUM(price),0) FROM balance WHERE destiny_bank = NEW.destiny_bank)
	)
	WHERE banks.bank_name = NEW.destiny_bank;
END$$
 
DELIMITER ;