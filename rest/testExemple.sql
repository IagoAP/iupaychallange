INSERT INTO banks (bank_name) VALUES ('A');
INSERT INTO banks (bank_name) VALUES ('B');
INSERT INTO banks (bank_name) VALUES ('C');
INSERT INTO banks (bank_name) VALUES ('D');
INSERT INTO banks (bank_name) VALUES ('E');

INSERT INTO payments (origin_bank, destiny_bank, price) VALUES ('A', 'B', 10);
INSERT INTO payments (origin_bank, destiny_bank, price) VALUES ('A', 'D', 20);
INSERT INTO payments (origin_bank, destiny_bank, price) VALUES ('C', 'A', 30);
INSERT INTO payments (origin_bank, destiny_bank, price) VALUES ('B', 'D', 40);
INSERT INTO payments (origin_bank, destiny_bank, price) VALUES ('D', 'C', 80);
INSERT INTO payments (origin_bank, destiny_bank, price) VALUES ('D', 'B', 30);
INSERT INTO payments (origin_bank, destiny_bank, price) VALUES ('E', 'A', 120);