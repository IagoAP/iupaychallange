DROP DATABASE IF EXISTS iupay;

CREATE DATABASE iupay;

USE iupay;

create table banks (
	       id INT(10) UNSIGNED AUTO_INCREMENT , 
	bank_name VARCHAR(255) NOT NULL, 
	      sum DOUBLE(10,2) NOT NULL DEFAULT 0.00,
	      
	primary key(
		id, 
		bank_name
	)
);

CREATE INDEX bank_name ON banks(bank_name);

ALTER TABLE banks ADD UNIQUE (bank_name);

create table payments (
	          id INT(10) UNSIGNED AUTO_INCREMENT , 
	 origin_bank VARCHAR(255) NOT NULL, 
	destiny_bank VARCHAR(255) NOT NULL, 
	       price DOUBLE(10,2) UNSIGNED NOT NULL, 
	
	primary key(id)
);

alter table payments add constraint bank_origin foreign key ( origin_bank ) references banks ( bank_name );

alter table payments add constraint bank_destiny foreign key ( destiny_bank ) references banks ( bank_name );

CREATE TABLE balance ( 
	origin_bank VARCHAR(255) NOT NULL, 
	destiny_bank VARCHAR(255) NOT NULL, 
	price DOUBLE(10,2) SIGNED NOT NULL 
);

alter table balance add constraint bank_fisrt foreign key ( origin_bank ) references banks ( bank_name );

alter table balance add constraint bank_second foreign key ( destiny_bank ) references banks ( bank_name );